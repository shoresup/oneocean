module("luci.controller.gateway.gatewaycontrol",package.seeall)

function index()
	page = entry({"admin", "gateway"},firstchild(), _("Gateway"),20)
	page.sysauth = "gateway"
	page.dependent = false	
	page = entry({"gateway","firewall"}, call("action_firewall"),"",10)
	page.sysauth = "gateway"
	page.dependent = false
	page = entry({"gateway","readfirewall"}, call("read_firewall"),"",10)
    page.sysauth = "gateway"
	page.dependent = false	
	page = entry({"gateway","readprotocols"}, call("read_protocols"),"",10)
    page.sysauth = "gateway"
	page.dependent = false	
	page = entry({"gateway","changepassword"},call("action_changepassword"),"",10)
	page.sysauth = "gateway"
	page.dependent = false
	page = entry({"gateway","backup"}, call("action_backup"),"Backup",10)
	page.sysauth = "root"
	page.dependent = false
	page = entry({"gateway","backup"}, call("action_backup"),"Backup",10)
	page.sysauth = {"gateway","root"}
	page.dependent = false
	page = entry({"gateway","rollback"}, call("action_rollback"),"Rollback",10)
	page.sysauth = "root"
	page.dependent = false
	page = entry({"gateway","upgrade"}, call("action_upgrade"),"Upgrade",10)
	page.sysauth = "root"
	page.dependent = false
	page = entry({"gateway","latestbackup"}, call("action_backupgetlatest"),"Backup",10)
	page.sysauth = "root"
	page.dependent = false
	page = entry({"admin","gateway","password"},template("gateway/password"),_("Change Password"),10)
	page.sysauth = "gateway"
	page.dependent = false
	page = entry({"admin","gateway","antivirus"},call("action_antiviruslog"),_("Anti Virus"),10)
	page.sysauth = "gateway"
	page.dependent = false
	page = entry({"admin", "gateway", "network"}, alias("admin","network","network"), _("Interfaces"), 50)
	page.sysauth = "gateway"
	page = entry({"admin", "gateway", "ipwhitelist"},call("action_ipwhitelist"),_("IP Whitelist"), 60)
	page.dependent = false	
	page = entry({"gateway","getipwhitelist"}, call("action_getipwhitelist"),"Whitelist",10)
	page.dependent = false
	page = entry({"gateway","saveipwhitelist"}, call("action_saveipwhitelist"),"Whitelist",10)
	page.dependent = false

end

function check_auth()
	local ubus = require "ubus"
	local conn = ubus.connect()
	local username = ""
	if not conn then
		error("connection error")
	else
		local status = conn:call("session", "list", {ubus_rpc_session = luci.http.getcookie("sysauth")})
        if status then
		    username  = status.data.username
			if username == "gateway"  or username=="root" then
				return true
			end
        else
			return false
		end
	end
	return false
end


function action_backup()
	luci.sys.exec("/root/scripts/backupconfig.sh")
end

function action_rollback()
	luci.sys.exec("/root/scripts/disconnectsound.sh")
	luci.sys.exec("/root/scripts/rollbackconfig.sh")
	http.redirect(luci.dispatcher.build_url('admin/gateway/flashops'))
end

function action_upgrade()
	luci.sys.exec("/root/scripts/swupdate.sh exec")
	http.redirect(luci.dispatcher.build_url('admin/gateway/flashops'))
end

function action_backupgetlatest()
	local backuplist = luci.sys.exec("ls -lt /root/backup|head -1|awk -F\" \" \'{print $9}\'")
	luci.http.write(backuplist)
	luci.http.write("---")
	local lines = {}
	local nooflines=0
	local releasestring = luci.sys.exec("/root/scripts/swupdate.sh get")
	for s in releasestring:gmatch("[^\r\n]+") do
		table.insert(lines, s)
		nooflines = nooflines+1
	end
	if nooflines>1
	then
		local version = lines[1]
		local gitversion = lines[2]:sub(0,8)
		luci.http.write("found version:"..version.." buildnr:"..gitversion)
	else
		luci.http.write("fail")
	end
end

function action_antiviruslog()
	local avlog = luci.sys.exec("freshclam --version");
	luci.template.render("gateway/antivirus", {syslog=avlog})
end

function action_ipwhitelist()
	luci.template.render("gateway/ipwhitelist")
end

function action_getipwhitelist()
	if check_auth() then
		whitelistuc = luci.sys.exec("cat /etc/config/ip_uc_whitelist")
		whitelist460 = luci.sys.exec("cat /etc/config/ip_460_whitelist")

		luci.http.write(whitelistuc)
		luci.http.write("---")
		luci.http.write(whitelist460)
	end
end

function action_saveipwhitelist()
	if check_auth() then
		luci.sys.exec("/root/scripts/backupconfig.sh")
		local ipucwhitelist = luci.http.formvalue("ipuclist")
		local file = io.open("/etc/config/ip_uc_whitelist","w")
		file:write(ipucwhitelist)
		file:close()
		local ip460whitelist = luci.http.formvalue("ip460list")
		local file = io.open("/etc/config/ip_460_whitelist","w")
		file:write(ip460whitelist)
		file:close()
		local result=luci.sys.exec("/root/scripts/firewallcontrol.sh")
		luci.http.write(result)
	end
end


function action_connectionlog()
	local connlog = luci.sys.exec("cat /var/log/ulogd_syslogemu.log");
	luci.template.render("gateway/connlog", {syslog=connlog})
end

function action_changepassword()
	if check_auth() then
		local newpassword = luci.http.formvalue("password")
		luci.sys.exec("/root/scripts/changepassword.sh \""..newpassword.."\"")
	end
end

function action_firewall()

	local ruletochange = luci.http.formvalue("rule")
	local action = luci.http.formvalue("action")
	
	local utl = require "luci.util"
	
	if ruletochange=='openvpn' and action=='1' then	
		luci.sys.exec("/etc/init.d/openvpn start");
	else
		luci.sys.exec("/etc/init.d/openvpn stop");
	end
	
	if action=='1' then
		luci.sys.exec("/root/scripts/firewallrule.sh "..ruletochange.." enable")
		luci.sys.exec("/root/scripts/firewallrule.sh dns enable")
		luci.sys.exec("/root/scripts/phaser.sh")
	else
		luci.sys.exec("/root/scripts/disconnectsound.sh")
		luci.sys.exec("/root/scripts/firewallrule.sh "..ruletochange.." disable")
		luci.sys.exec("/root/scripts/firewallrule.sh dns disable")
	end	
end


function read_firewall()
	local rule = luci.http.formvalue("rule")
	local ruletochange = luci.http.formvalue("rule")
	local ruleinfo = {}

	local result=luci.sys.exec("/root/scripts/firewallrule.sh "..ruletochange.." get")
	if string.find(result,"enabled") then
		ruleinfo["rulestate"]="1"
	else	
		ruleinfo["rulestate"]="0"
	end
	
	ruleinfo["usage"] = luci.model.uci:get("gateway",rule,"usage")
	ruleinfo["icon"] = luci.model.uci:get("gateway",rule,"icon")
	ruleinfo["themecolor"] = luci.model.uci:get("gateway",rule,"themecolor")
	ruleinfo["inactivetimer"] = luci.model.uci:get("gateway",rule,"inactivetimer")
	ruleinfo["activetimer"] = luci.model.uci:get("gateway",rule,"totaltimer")
	ruleinfo["title"] = luci.model.uci:get("gateway",rule,"title")	
	ruleinfo["port"] = luci.model.uci:get("gateway",rule,"port")
	luci.http.write_json(ruleinfo)

end

function read_protocols()
	local protocollist= {}
	luci.model.uci:foreach("gateway", "gatewayrule", function (s)
														table.insert(protocollist,s[".name"])	
													 end  )
													
	luci.http.write_json(protocollist)
end
