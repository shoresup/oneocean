m= Map("shore2ship", translate("Shore2ship Settings"), translate("Set up shore2ship specific settings here")) -- cbi_file is the config file in /etc/config
f = m:section(TypedSection, "status", "Current monthly usage");f.anonymous=true
h = f:option(DummyValue, "usage","Total usage this month (KB)")
d = m:section(TypedSection, "limits", "Monthly limit settings (3G/4G)");d.anonymous=true  -- info is the section called info in cbi_file
a = d:option(Value, "datalimit", "Max. usage (KB) )"); a.optional=false; a.rmempty = false;  -- name is the option in the cbi_file
c = d:option(DynamicList, "rules","Disable these rules when limit is reached"); c.optional=false;c.delimiter=";"
g = m:section(TypedSection, "general", "Usage monitoring"); g.anonymous=true
d = g:option(Value, "serial","Shore2ship serial"); d.optional=false;
e = g:option(Value, "url","URL for posting usage data"); e.optional=false;
return m
