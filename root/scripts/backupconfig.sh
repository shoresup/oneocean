#!/bin/sh
#
# version 1.0: december 2020
#
# copyright (c) Shore2ship
#
date=$(date '+%d_%m_%Y_%H_%M')
filename="Backup_$date.tar.gz"
echo $filename
sysupgrade --create-backup - >/root/backup/$filename
