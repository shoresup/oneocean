#!/bin/sh
#
# version 1.0: december 2020
#
# copyright (c) Shore2ship
#
maxtotaltime=$(uci get gateway.limits.maxtotaltime)
maxinactivetime=$(uci get gateway.limits.maxinactivetime)

uci show gateway|grep '.port='|awk -F"." '{print $2}'| tr ' ' '\n'|
while read gwname
do
	protport=$(uci get gateway.$gwname.port)
	protindex=$(uci show firewall|grep $gwname|awk -F"[" '{print $2}'|awk -F"]" '{print $1}')
	protopen=$(uci get firewall.@rule[$protindex].enabled)
	echo index=$protindex open=$protopen
	echo gateway=$gwname port=$protport
	usagelines=$(cat /proc/net/nf_conntrack|grep dport=$protport )
	echo $usagelines
	connectionbytes=$( echo "$usagelines"  | wc -c )
	if [ $connectionbytes -gt 1 ] || [ $protopen -eq 1 ]
	then
		connectionisup=true
	else
		connectionisup=false
		uci set gateway.$gwname.inactivetimer=0
		uci set gateway.$gwname.totaltimer=0
		uci set gateway.$gwname.usage=0
		continue
	fi
	echo $gwname connectionisup:$connectionisup
	usage=0
	bytes=$(echo $usagelines|awk -F" " '{print $17}'|awk -F"=" '{print $0}')
	echo bytes:$bytes
	usage=$bytes
	oldusage=$(uci get gateway.$gwname.usage)
	totaltimer=$(uci get gateway.$gwname.totaltimer)
	inactivetimer=$(uci get gateway.$gwname.inactivetimer)
	newtimer=$(( $totaltimer + 1 ))
	if [ $usage = $oldusage ] 
	then
		inactivetimer=$(( $inactivetimer + 1 ))
		if [ $inactivetimer -gt $maxinactivetime ]
		then
			echo 'inactive timer exceeded: connection should drop'
			/root/scripts/firewallrule.sh $gwname disable
			/root/scripts/firewallrule.sh dns disable
			if [ $gwname = 'openvpn' ]
			then	
				echo 'stopping openvpn'
				/etc/init.d/openvpn stop
			fi
			beep
		fi
		uci set gateway.$gwname.inactivetimer=$inactivetimer 
	else
		#we have data usage, so reset inactivetimer
		uci set gateway.$gwname.inactivetimer=0
	fi
	totaltimer=$(( $totaltimer + 1 ))
	if [ $totaltimer -gt $maxtotaltime ]
	then
		echo 'total time exceeded: connection should drop'
		/root/scripts/firewallrule.sh $gwname disable
		/root/scripts/firewallrule.sh dns disable
		if [ $gwname = 'openvpn' ]
		then	
				echo 'stopping openvpn'
				/etc/init.d/openvpn stop
		fi
		beep
		beep
		uci set gateway.$gwname.totaltimer=0
		uci set gateway.$gwname.inactivetimer=0
		uci set gateway.$gwname.usage=0
		totaltimer=0
	fi
	uci set gateway.$gwname.totaltimer=$totaltimer
	uci set gateway.$gwname.usage=$usage
done
uci commit
