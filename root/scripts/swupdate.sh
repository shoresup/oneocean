#!/bin/sh
#
# version 1.0: december 2020
#
# copyright (c) Shore2ship
#
problems=1
lines=$(cat /usb/update/manifest.txt) 
for line in $lines
do
	filename=$(echo "$line"| awk -F':' '{ print $1 }')
	todir=$(echo "$line"| awk -F':' '{ print $2 }')
	permission=$(echo "$line"| awk -F':' '{ print $3 }')
	filehash=$(echo "$line"| awk -F':' '{ print $4 }')
	hash=$(sha256sum /usb/update/$filename|awk -F' ' '{print $1}')
	if [ $hash = $filehash ]
	then 
		if [ $1 = 'exec' ]
		then 
			cp /usb/update/$filename $todir/$filename
			chmod $permission $todir/$filename	
		fi
		problems=0
	else
		problems=1
		break
	fi
done	
if [ $problems = 0 ]
then	
	if [ $1 = 'get' ]
	then
		cat /usb/update/s2s_release
	else
		cp /usb/update/s2s_release /etc/s2s_release 
		beep -f 500 -r 5 -d 100
	fi
else 
		echo "fail"
fi
