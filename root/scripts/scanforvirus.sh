#!/bin/sh
# 
# scanforvirus.sh
#
# version 1.0: december 2020
#
# copyright (c) Shore2ship
#
#
scanresult=$(clamdscan --move=/root/quarantine /data| grep FOUND >/dev/null && echo FOUND || echo OK)
if [ $scanresult == FOUND ];
then
	/root/scripts/alarm.sh
fi