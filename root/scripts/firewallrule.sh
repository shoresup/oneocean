#!/bin/sh
#
# firewallrule.sh
#
# version 1.0: august 2016
# version 1.1: february 2020.
# 
# feb 2020: bugfix reading enabled status (when uci enabled line does not exist) 
#
# copyright (c) Shore2ship
#
index=$(uci show firewall|grep -w "$1"|awk -F"[" '{print $2}'|awk -F"]" '{print $1}')
if [ -z $index ]
then
	echo "$1"
	echo rule not found so exiting...
	exit
fi
somethinghaschanged=0
#uci set firewall.@rule[$index].enabled=0
enabled=$(uci get firewall.@rule[$index].enabled 2>>/dev/null)
if [ -z $enabled ]
then 
	echo rule is already enabled
	enabled=true	
else
	if [ $enabled = 0 ]
	then
		enabled=false
		echo rule found, is currently disabled
	else
		enabled=true
		echo rule found, is currrently enabled
	fi	
fi
if [ $2 = enable ]
then
	if [ $enabled = false ]  
	then
		uci set firewall.@rule[$index].enabled=1
		uci commit firewall
		echo rule $1 changing from disabled to enabled 
		somethinghaschanged=1
	fi
elif [ $2 = get ] 
then
	uci get firewall.@rule[$index].enabled
elif [ $2 = disable ]
then
	if [ $enabled = true ] 
	then
		uci set firewall.@rule[$index].enabled=0
		echo rule $1 changing from enabled to disabled
		uci commit firewall
	fi
	somethinghaschanged=1
fi
if [ $somethinghaschanged = 1 ]
then
	echo firewall restart required
	/etc/init.d/firewall restart
fi
