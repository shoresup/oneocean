#!/bin/sh
#
# firewallcontrol.sh
#
# version 1.0: december 2020
#
# copyright (c) Shore2ship
#
sourceips=$(cat /etc/config/ip_uc_whitelist)
destips=$(cat /etc/config/ip_460_whitelist)
sourceports=$(uci show shore2ship|grep ports_uc|awk -F"=" '{print $2}')
destports=$(uci show shore2ship|grep port_460|awk -F"=" '{print $2}')

#check/add/delete ip addresses on the uncontrolled side
for port in $sourceports
do	
	rulename=$(echo s2sauto_uc_$port | tr -d "'")
	srcport=$(echo $port|tr -d "'")
	#check if rule exists, if rule does not exist we add it
	index=$(uci show firewall|grep -w "$rulename"|awk -F"[" '{print $2}'|awk -F"]" '{print $1}')
	if [ -z $index ]
	then
		echo "adding rule $rulename"
		uci add firewall rule
		uci set firewall.@rule[-1].name=$rulename
		uci set firewall.@rule[-1].src='uc_zone'
		for ip in $sourceips
		do
			#src_ip=$(echo $ip|tr -d "'")
			uci add_list firewall.@rule[-1].src_ip=$ip
		done
		uci set firewall.@rule[-1].dest_port=$srcport
		uci set firewall.@rule[-1].target='ACCEPT'
	else
		#rule already exists, we need to check if all ips are added and if any are removed

		#check if no ips in whitelist at all then we set rule to REJECT
		noofips=$(echo $sourceips|wc -w)
		if [ $noofips -eq "0" ]
		then
			uci set firewall.@rule[$index].target='REJECT'
		else	
			uci set firewall.@rule[$index].target='ACCEPT'
		fi

		
		actualsourceips=$(uci show firewall.@rule[$index].src_ip|grep src_ip|awk -F"=" '{print $2}')
		for ip in $sourceips
		do
			#src_ip=$(echo $ip|tr -d "'")
			foundip=false
			for actualip in $actualsourceips
			do
				actualsrc_ip=$(echo $actualip|tr -d "'")
				#echo $actualsrc_ip // $ip
				if [ $ip = $actualsrc_ip ]
				then
					foundip=true
					echo "found $ip $srcport"
					break
				fi
			done
			
			echo $foundip
			if [ $foundip = false ]
			then
				echo $index
				uci add_list firewall.@rule[$index].src_ip=$ip
			fi
		done
		#check for ip addresses to be removed
		for ip in $actualsourceips
		do
			srcip=$(echo $ip|tr -d "'")
			if echo $sourceips | grep -q $srcip;
			then
				echo
			else
				echo not found: $srcip
				uci del_list firewall.@rule[$index].src_ip=$srcip
			fi
		done
	fi
done

#check/add/delete ip addresses on the 460 side
for port in $destports
do	
	rulename=$(echo s2sauto_460_$port | tr -d "'")
	srcport=$(echo $port|tr -d "'")
	#check if rule exists, if rule does not exist we add it
	index=$(uci show firewall|grep -w "$rulename"|awk -F"[" '{print $2}'|awk -F"]" '{print $1}')
	if [ -z $index ]
	then
		echo "adding rule $rulename"
		uci add firewall rule
		uci set firewall.@rule[-1].name=$rulename
		uci set firewall.@rule[-1].src='460_zone'
		for ip in $sourceips
		do
			#src_ip=$(echo $ip|tr -d "'")
			uci add_list firewall.@rule[-1].src_ip=$ip
		done
		uci set firewall.@rule[-1].dest_port=$srcport
		uci set firewall.@rule[-1].target='ACCEPT'
	else
		#rule already exists, we need to check if all ips are added and if any are removed

		#check if no ips in whitelist at all then we set rule to REJECT
		noofips=$(echo $destips|wc -w)
		if [ $noofips -eq "0" ]
		then
			uci set firewall.@rule[$index].target='REJECT'
		else	
			uci set firewall.@rule[$index].target='ACCEPT'
		fi


		actualsourceips=$(uci show firewall.@rule[$index].src_ip|grep src_ip|awk -F"=" '{print $2}')
		for ip in $destips
		do
			#src_ip=$(echo $ip|tr -d "'")
			foundip=false
			for actualip in $actualsourceips
			do
				actualsrc_ip=$(echo $actualip|tr -d "'")
				#echo $actualsrc_ip // $ip
				if [ $ip = $actualsrc_ip ]
				then
					foundip=true
					echo "found $ip $srcport"
					break
				fi
			done
			
			echo $foundip
			if [ $foundip = false ]
			then
				echo $index
				uci add_list firewall.@rule[$index].src_ip=$ip
			fi
		done
		#check for ip addresses to be removed
		for ip in $actualsourceips
		do
			srcip=$(echo $ip|tr -d "'")
			if echo $destips | grep -q $srcip;
			then
				echo
			else
				echo not found: $srcip
				uci del_list firewall.@rule[$index].src_ip=$srcip
			fi
		done
	fi
done


uci commit firewall
/etc/init.d/firewall restart