#!/bin/sh
#
# version 1.0: december 2020
#
# copyright (c) Shore2ship
#
filename=$(ls -t /root/backup|head -1)
echo $filename
sysupgrade --restore-backup /root/backup/$filename
